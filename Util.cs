namespace dotnetcore{
    public enum OperacoesBancarias
    {
        Inicio,
        ConsultarSaldo,
        RealizarDeposito,
        RealizarSaque,
        Sair,
        Erro
    }
}