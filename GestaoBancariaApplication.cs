using System;

namespace dotnetcore
{
    public class GestaoBancariaApplication
    {
        public int Menu(ContaBancaria contaBancaria)
        {
            Console.WriteLine("=============================================");
            Console.WriteLine("Qual operação deseja realizar?");
            Console.WriteLine("1 - Consultar Saldo");
            Console.WriteLine("2 - Realizar Depósito");
            Console.WriteLine("3 - Relizar Saque");
            Console.WriteLine("=============================================");
            
            try
            {
                var operacao = Convert.ToInt32(Console.ReadLine());           

                switch (operacao)
                {
                    case (int)OperacoesBancarias.ConsultarSaldo:
                        contaBancaria.ConsultarSaldo();
                        break;

                    case (int)OperacoesBancarias.RealizarDeposito:
                        Console.WriteLine("Informe o valor a ser depositado:");
                        var valorDeposito = Console.ReadLine();
                        contaBancaria.RealizarDeposito(valorDeposito);
                        break;

                    case (int)OperacoesBancarias.RealizarSaque:
                        Console.WriteLine("Informe o valor a ser sacado:");
                        var valorSaque = Console.ReadLine();
                        contaBancaria.RealizarSaque(valorSaque);
                        break;

                    default:
                        Console.WriteLine("Fim da operação.");
                        return (int)OperacoesBancarias.Sair;
                }

                Console.WriteLine("=============================================");
                return (int)OperacoesBancarias.Erro;
            }
            catch (System.Exception)
            {
                return (int)OperacoesBancarias.Erro;
            }
        }
    }
}