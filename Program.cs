﻿using System;

namespace dotnetcore
{
    class Program
    {
        private static GestaoBancariaApplication gestaoBancariaApplication = new GestaoBancariaApplication();
        static void Main(string[] args)
        {

            Console.WriteLine("Bem Vindo ao Banco Cotemig!");
            Console.WriteLine("Deseja criar uma conta? S/N");
            var resposta = Console.ReadLine();

            if (resposta.ToUpper().Equals("S"))
            {
                ContaBancaria contaBancaria = CriarContaBancaria();
                MenuContaBancaria(contaBancaria);
            }

            Console.Write($"{Environment.NewLine}Aperte qualquer botão para sair...");
            Console.ReadKey(true);

        }

        private static void MenuContaBancaria(ContaBancaria contaBancaria)
        {
            Console.WriteLine("=============================================");

            Console.WriteLine($"Bem-vindo(a) {contaBancaria.Nome} ao banco Cotemig");

            int operacao = (int)OperacoesBancarias.Inicio;
            do
            {
                operacao = gestaoBancariaApplication.Menu(contaBancaria);
            } while (operacao < (int)OperacoesBancarias.Sair);

            Console.WriteLine($"Obrigado por usar o banco Cotemig {contaBancaria.Nome}");
        }

        private static ContaBancaria CriarContaBancaria()
        {
            Console.WriteLine("Informe o seu nome");
            var nome = Console.ReadLine();

            Console.WriteLine("Informe sua data de nascimento:");
            DateTime.TryParse(Console.ReadLine(), out var resultado);

            var contaBancaria = new ContaBancaria(nome, resultado);
            return contaBancaria;
        }
    }
}
