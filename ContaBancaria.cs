using System;

namespace dotnetcore
{
    public class ContaBancaria
    {
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public DateTime DataCriacao {get; set;}
        public double Saldo { get; private set; }
        
        public ContaBancaria(string nome, DateTime dataNascimento)
        {
            Nome = nome;
            DataNascimento = dataNascimento;
            DataCriacao = DateTime.Now;
        }

        public void RealizarDeposito (string valorDeposito) 
        {
            Saldo =+ Convert.ToDouble(valorDeposito);
            Console.WriteLine("Saldo atual: " + Saldo);
        }

        public void RealizarSaque (string valorSaque)
        {
            Saldo -= Convert.ToDouble(valorSaque);
            Console.WriteLine("Saldo atual: " + Saldo);
        }

        public void ConsultarSaldo()
        {
            Console.WriteLine("Saldo disponível: " + Saldo);
        }
    }
}